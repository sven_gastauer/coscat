---
title: "Modeling the backscattering by a prolate spheroidal copepod with a prolate spheroidal lipid (oilsac)"
author: "Dezhang Chu, Sven Gastauer"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Modeling the backscattering by a prolate spheroidal copepod with a prolate spheroidal lipid (oilsac)}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

## The DWBA model  

The backscattering of a whole copepod (body flesh + lipid) can be approximated by the volume integration based on the Distorted Wave Born Approximation (DWBA):  

\begin{equation} f_{bs}^{DWBA}=\frac{k_1^2}{4\pi}\int_{v_0}{[h^2(v_0)\gamma_k(v_0)+\hat{k_i}* \hat{k_s}\gamma_k(v_0)]}e^{ik_e(v_0)( \hat{k_i}*\hat{k_s}*r_0)} \label{eq:DWBA}\tag{1}\end{equation} 
where   are the wave number in medium 1 (surrounding water),  is the position inside the scatterer, and   and  are unit vectors of the incident and scattered wavenumbers, respectively.  
Note that because of inhomogeneity inside the volume,  the parameters wave number,  sound speed contrast,  relative differences of compressibility and density,   respectively, are all functions of positions inside the integration volume,   Assuming an prolate spheroidal lipid content, or oil sac, inside the copepod body (Fig. 1), using (A6), (A17), and (A22), Eq. (1) can be expressed as:  

\begin{equation} f_{bs}^{DWBA}=\frac{k_1^2}{4\pi}(h^2_f\gamma_{k_f})\int_c^cdz_0\int^{r_f(z_0)}_{r_{lipid}(z_0)} r_odr\int^{2\pi}_{0}e^{ikr_0\sqrt{\prod_x^2+e^2_{ba_f}\prod_y^2}cos(\phi_0-\phi')+ik_f\prod_zz_0}d\phi_0 + \\
\frac{k_1^2}{4π}(h_{lipid}^2 γ_{k_{lipid}}-γ_{\rho_{lipid}}) \int_{-c}^{c}d_{z_0} \int_0^{r_{lipid}(z_0)}r_0 dr_0  \int_0^{2π}e^{ik_{lipid} r_0 \sqrt{\prod^2_\pi +e^2_{ba_{lipid}}\prod^2_y}cos(\phi_o-\phi')+ik_{lipid}\prod_zz_0}d\phi_0  \\ 
= \frac{k_1^2 L}{8π} (h_f^2 γ_{k_f}-γ_{\rho_f}) \left(r_f^2  \frac{j_1 (2k_f r_f \sqrt{sin^2\theta_s+e_f^2cos^2(\theta_s)})}{2k_f r_f \sqrt{sin^2\theta_s+e_f^2cos^2(\theta_s)}} - r^2_{lipid}\frac{j_1 (2k_f r_f \sqrt{sin^2\theta_s+e_{lipid}^2cos^2(\theta_s)})}{2k_f r_f \sqrt{sin^2\theta_s+e_{lipid}^2cos^2(\theta_s)}}\right) + \\

\frac{k_1^2 r_{lipid}L}{8π} (h_{lipid}^2 γ_{k_{lipid}}-γ_{\rho_{lipid}}) \frac{j_1 (2k_{lipid} r_{lipid} \sqrt{sin^2\theta_s+e_{lipid}^2cos^2(\theta_s)})}{2k_{lipid} r_{lipid} \sqrt{sin^2\theta_s+e_{lipid^2cos^2(\theta_s)}}}\label{eq:DWBA_expand}\tag{2}\end{equation} 

where subscripts *f* and *lipid* corresponding material properties associated with the body flesh and lipid of the copepod, respectively. With g and h represent density and sound speed contrasts, we have:

$k_f = \frac{k_1}{h_f}$; $k_{lipid}=\frac{k_1}{h_{lipid}}$; $e_f=\frac{2r_f}{L}$;$e_{lipid}=\frac{2r_{lipid}}{L}$  
and  
$\gamma_{k_f}-\gamma_{\rho_f}=\frac{1-g_fh^2_f}{g_fh^2_f}-\frac{g_f-1}{g_f}$; $\gamma_{k_{lipid}}-\gamma_{\rho_{lipid}}=\frac{1-g_{lipid}h^2_{lipid}}{g_{lipid}h^2_{lipid}}-\frac{g_{lipid}-1}{g_{lipid}}$  
The first two terms of Eq. (2) are contribution from the flesh while the last term is from the lipid. Note that $\theta=0$ corresponds to end-on incidence and  $\theta=90^\circ$ corresponds to broadside incidence (Fig. 1).

![*Figure 1. Geometry of the scattering by a copepod with lipid content.  Shapes of both the body and lipid content are assumed to be spheroidal.*](shape.png){align=centre}  


## The model in *coscat*  

*coscat* computes target strength (TS) estimates [$dB$ $re$ $m^-2$] for a hypothetical copepod with a prolate spheriodal body shape and a prolate spheroidal oil sac for a defined frequency or frequency range at a given orientation, with given soundspeed and density contrasts.  
In *coscat* the following settings can be defined:  

* *f* [numeric] frequencies in Hz - default: seq(0.01,1.2, by=0.01)*1000000 for 1 - 1200 kHz
* *c* [numeric] sound velocity in m/s
* *theta* [numeric] tilt angle in degrees
* *L* [numeric] body length in m  - default: 2.45/1000 for a size of 2.45 mm
* *rf* [numeric] radius of the fluid body in m - default: 0.806/1000 for 0.806 mm
* *rlipid* [numeric] radius of the lipid sac in m - default:  0.75/1000 for 0.75 mm
* *Llipid* [numeric] length of the lipid body - default: 0.805/1000 for 0.805 mm
* *gf* [numeric] density contrast g of the fluid body, compared to the surrounding fluid (sea water) - defualt: 0.990
* *hf* [numeric] sound speed contrast h of the fluid body compared to the surrounding fluid (sea water) - default: 1.0467
* *glipid* [numeric] density contrast g of the lipid, compared to the surrounding fluid (sea water) - default: 0.9227
* *hlipid* [numeric] soundspeed contrast h of the lipid, compared to the surrounding fluid (sea water) - default: 0.9025
* *plot.out* [boolean] If TRUE, a plot will be produced at the end, if false, no plot will be produced
  
The model is implemented in *coscat*  as a function called `cDWBA()`:  

```{r}
library(coscat)
ts <- coscat::cDWBA()
```


As per usual R function, all settings can be set within the function call, e.g. applying a 90 degrees rotation:  

```{r}
ts90 <- coscat::cDWBA(theta=90)
```

## Running the shiny app

The copepod DWBA model can be easily parametrised through a shiny web application.  
The web application can be called through:  

```{r, eval=FALSE}
coscat::coscatApp()
```
  
  
This should launch a web application:  

![*Figure 2. coscat web application.*](app.png){align=centre, width=600px}  
