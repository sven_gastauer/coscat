# coscat - Modeling the backscattering by a prolate spheroidal copepod with a prolate spheroidal lipid (oilsac)

Sven Gastauer (1,2); Dezhang Chu (3)

1) Antarctic Climate and Ecosystem Cooperative Research Centre, University of Tasmania, Private Bag 80, Hobart, Tasmania, 7001, sven.gastauer@utas.edu.au  
2) Australian Antarctic Division, 203 Channel Highway, Kingston, TAS 7050, Australia  
3) Northwest Fisheries Science Center, National Marine Fisheries Service, National Oceanic and Atmospheric Administration, 2725 Montlake Boulevard East, Seattle, Washington 98112, USA  
	
# Overview  
*coscat* is an implementation of the DWBA (Distorted Wave Born Approximation), modeling the acoustic backscatter of a prolate spheroidal copepod with a prolate spheroidal lipid (oilsac). The user can vary the body shape, the oil sack shape, orientation, the density and sound speed contrast of the body and the oil compared to the surrounding fluid, the sound speed of the surrounding fluid and the output frequencies.  
![shape](https://bitbucket.org/sven_gastauer/coscat/raw/589ea9ce828525b1a700a59270e4470100c8e7b7/vignettes/shape.png)

#  Copyright and Licence  
  

Copyright (C) 2019 Sven Gastauer, Dezhang Chu.
	
This file is part of coscat and the larger Zooscat project.
	
*coscat* is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.    	
*coscat* is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.    
  
You should have received a copy of the GNU General Public License along with *coscat*.  If not, see <http://www.gnu.org/licenses/>. This package is open for community development and we encourage users to extend the package as they need. We are not liable for  any losses when using *coscat*.


# Installation  

*coscat* can be installed form BitBucket. This requires the [devtools](https://cran.r-project.org/web/packages/devtools/index.html) package.

``` r
# The package can be installed from BitBucket:
# install.packages("devtools")
#Install the package from bitbucket
devtools::install_bitbucket("sven_gastauer/coscat")
```
## Dependencies  

ZooScatR currently depends on: 
  
* [shiny](https://shiny.rstudio.com/) - to build the shiny app
* [knitr](https://yihui.name/knitr/) - transparent engine for dynamic report generation with R
  
These dependencies should be installed automatically, if unavailable when ZooScatR is installed. If not, the missing libraries can be installed through:  

``` r
packages <- c("shiny", "knitr", "devtools")
if (length(setdiff(packages, rownames(installed.packages()))) > 0) {
  install.packages(setdiff(packages, rownames(installed.packages())))}
```  
If you get a message such as:  
```WARNING: Rtools is required to build R packages, but is not currently installed. ```  

Install Rtools by downloading the required binary package from cran:  

* [Windows users](https://cran.r-project.org/bin/windows)  
* [Linux users](https://cran.r-project.org/bin/linux)  
* [MacOsX](https://cran.r-project.org/bin/macosx)
* [MacOs](https://cran.r-project.org/bin/macos)  


# Usage - Quick Start  
  
For a quick start, a minimal example:  
``` r
library(coscat)
#run the coscat model from console
coscat::cDWBA()
```

This prodcues the target strength estiamtes for a copepod with the following settings:  

* f=seq(0.01,1.2, by=0.01)*1000000
* c = 1460
* theta = 0
* L = 2.45/1000
* rf = 0.806/1000
* rlipid =  0.75/1000
* Llipid = 0.805/1000
* gf = 0.990
* hf = 1.0467
* glipid = 0.9227
* hlipid = 0.9025
* plot.out = TRUE

Each of these setting can be modified individually.

## Web Application  

The *coscat* shiny web application can be run through:  

``` r
coscat::coscatApp()
```


![coscat app](https://bitbucket.org/sven_gastauer/coscat/raw/589ea9ce828525b1a700a59270e4470100c8e7b7/vignettes/app.png)